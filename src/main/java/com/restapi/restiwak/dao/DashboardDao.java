package com.restapi.restiwak.dao;

public interface DashboardDao {

    public Integer jmlKepalaKeluarga();
    public Double jmlKasMasuk();
    public Integer jmlKasKeluar(String rt);
    public Double jmlSaldoTotal();

    public Long jmlBelumBayar(String rt);
    public Long jmlSudahBayar(String rt);
}
