package com.restapi.restiwak.dao;

import com.restapi.restiwak.pojo.User;
import com.restapi.restiwak.pojo.UserRole;

import java.util.List;

public interface UserDao {
    public List<UserRole> userRoleList();
    public User getUserbyEmailandPassword(User user);
    public User getUserbyEmailandPassword(String email, String password);
    public List<User> userList();
    public List<User> userList(User user);
    public List<User> userListByRt(String rt);
    public User userMyId();
}
