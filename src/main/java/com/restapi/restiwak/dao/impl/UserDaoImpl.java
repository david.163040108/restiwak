package com.restapi.restiwak.dao.impl;

import com.restapi.restiwak.dao.UserDao;
import com.restapi.restiwak.pojo.User;
import com.restapi.restiwak.pojo.UserRole;
import org.springframework.stereotype.Repository;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import java.util.List;

@Repository
public class UserDaoImpl implements UserDao {
    @PersistenceContext
    EntityManager entityManager;

    @Override
    public List<UserRole> userRoleList() {
        return null;
    }

    @Override
    public User getUserbyEmailandPassword(User user) {
        return null;
    }

    @Override
    public User getUserbyEmailandPassword(String email, String password) {
        CriteriaBuilder criteriaBuilder = entityManager.getCriteriaBuilder();
        CriteriaQuery<User> userCriteriaQuery =criteriaBuilder.createQuery(User.class);
        Root<User> root = userCriteriaQuery.from(User.class);
        Predicate emails =criteriaBuilder.equal(root.get("email"),email);
        Predicate pass = criteriaBuilder.equal(root.get("password"),password);
        Predicate andQuery = criteriaBuilder.and(emails,pass);
        userCriteriaQuery.select(root);
        userCriteriaQuery.where(andQuery);
        User user = entityManager.createQuery(userCriteriaQuery).getSingleResult();
        return user;
    }

    @Override
    public List<User> userList() {
        return null;
    }

    @Override
    public List<User> userList(User user) {
        return null;
    }

    @Override
    public List<User> userListByRt(String rt) {
        return null;
    }

    @Override
    public User userMyId() {
        return null;
    }
}
