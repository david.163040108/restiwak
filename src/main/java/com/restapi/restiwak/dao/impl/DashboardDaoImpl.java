package com.restapi.restiwak.dao.impl;

import com.restapi.restiwak.dao.DashboardDao;
import com.restapi.restiwak.pojo.Pengeluaran;
import com.restapi.restiwak.pojo.TransaksiIuran;
import com.restapi.restiwak.pojo.User;
import org.springframework.stereotype.Repository;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.*;
import java.sql.Date;

@Repository
public class DashboardDaoImpl  implements DashboardDao {
    @PersistenceContext
    EntityManager entityManager;

    @Override
    public Integer jmlKepalaKeluarga() {
        return null;
    }

    @Override
    public Double jmlKasMasuk() {
        return null;
    }

    @Override
    public Integer jmlKasKeluar(String rt) {
        CriteriaBuilder criteriaBuilder = entityManager.getCriteriaBuilder();
        Expression<Date> date = criteriaBuilder.currentDate();
        Expression<Integer> month = criteriaBuilder.function("month", Integer.class, date);
        Expression<Integer> year = criteriaBuilder.function("year", Integer.class, date);


        CriteriaQuery<Integer> criteriaQuery = criteriaBuilder.createQuery(Integer.class);
        Root<Pengeluaran> root = criteriaQuery.from(Pengeluaran.class);
        Join<Pengeluaran, User> joinUser = root.join("user");
        Predicate user = criteriaBuilder.equal(joinUser.get("id"), rt);
        Predicate bulan = criteriaBuilder.equal(criteriaBuilder.function("MONTH", Integer.class, root.get("tanggalPengeluaran")),month);
        Predicate tahun = criteriaBuilder.equal(criteriaBuilder.function("YEAR", Integer.class, root.get("tanggalPengeluaran")),year);
        criteriaQuery.select(criteriaBuilder.sum(root.get("nominal")));
        criteriaQuery.where(user,bulan,tahun);
        TypedQuery<Integer> typedQuery = entityManager.createQuery(criteriaQuery);
        Integer sum =typedQuery.getSingleResult();
        System.out.println(sum);
        return sum;
    }

    @Override
    public Double jmlSaldoTotal() {

        return null;
    }

    @Override
    public Long jmlBelumBayar(String rt) {
        CriteriaBuilder criteriaBuilder = entityManager.getCriteriaBuilder();
        CriteriaQuery<Long> criteriaQuery =criteriaBuilder.createQuery(Long.class);
        Root<TransaksiIuran> root = criteriaQuery.from(TransaksiIuran.class);
        Join<TransaksiIuran, User> joinUser = root.join("user");
        Predicate user = criteriaBuilder.equal(root.get("idRt"), rt);
        Predicate status = criteriaBuilder.equal(root.get("active"), false);
        criteriaQuery.select(criteriaBuilder.count(root));
        criteriaQuery.where(user,status);
        TypedQuery<Long> typedQuery = entityManager.createQuery(criteriaQuery);
        Long count = typedQuery.getSingleResult();
        return count;
    }

    @Override
    public Long jmlSudahBayar(String rt) {
        CriteriaBuilder criteriaBuilder = entityManager.getCriteriaBuilder();
        Expression<Date> date = criteriaBuilder.currentDate();
        Expression<Integer> month = criteriaBuilder.function("month", Integer.class, date);
        Expression<Integer> year = criteriaBuilder.function("year", Integer.class, date);

        CriteriaQuery<Long> criteriaQuery =criteriaBuilder.createQuery(Long.class);
        Root<TransaksiIuran> root = criteriaQuery.from(TransaksiIuran.class);
        Join<TransaksiIuran, User> joinUser = root.join("user");
        Predicate user = criteriaBuilder.equal(root.get("idRt"), rt);
        Predicate status = criteriaBuilder.equal(root.get("active"), true);
        Predicate bulan = criteriaBuilder.equal(criteriaBuilder.function("MONTH", Integer.class, root.get("tanggalIuran")),month);
        Predicate tahun = criteriaBuilder.equal(criteriaBuilder.function("YEAR", Integer.class, root.get("tanggalIuran")),year);
        criteriaQuery.select(criteriaBuilder.count(root));
        criteriaQuery.where(user,status,bulan,tahun);
        TypedQuery<Long> typedQuery = entityManager.createQuery(criteriaQuery);
        Long count = typedQuery.getSingleResult();
        return count;
    }
}
