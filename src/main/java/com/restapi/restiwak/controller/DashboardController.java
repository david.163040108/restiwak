package com.restapi.restiwak.controller;

import com.restapi.restiwak.dao.DashboardDao;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

import java.math.BigDecimal;

@RestController
public class DashboardController {

    @Autowired
    private DashboardDao dashboardDao;
    @GetMapping("/kaseluar/{rt}")
    public Integer jmlkasKeluar(@PathVariable("rt") String rt){
        return dashboardDao.jmlKasKeluar(rt);
    }
    @GetMapping("/blumbayar{rt}")
    public Long jmlBelumBayar(@PathVariable("rt") String rt){
        return dashboardDao.jmlBelumBayar(rt);
    }

    @GetMapping("/sudahbayar{rt}")
    public Long jmlSudahBayar(@PathVariable("rt") String rt){
        return dashboardDao.jmlSudahBayar(rt);
    }
}
